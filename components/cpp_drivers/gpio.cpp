
#include <esp_log.h>

#include <gpio.hpp>

// **************************************************************************
// * Output
// **************************************************************************

EspDigitalOutput::EspDigitalOutput(int pin, bool defaultLevel)
    : EspDigitalOutput(pin, defaultLevel, GPIO_PULLUP_DISABLE, GPIO_PULLDOWN_DISABLE, false)
{
}

EspDigitalOutput::EspDigitalOutput(int pin, bool defaultLevel, gpio_pullup_t pullup, gpio_pulldown_t pulldown,
                                   bool openDrain)
    : _pin(pin)
{
    set(defaultLevel);

    gpio_config_t config{
        .pin_bit_mask{pinMask()},
        .mode{openDrain ? GPIO_MODE_OUTPUT_OD : GPIO_MODE_OUTPUT},
        .pull_up_en{pullup},
        .pull_down_en{pulldown},
        .intr_type{GPIO_INTR_DISABLE},
    };
    ESP_ERROR_CHECK(gpio_config(&config));
}

void EspDigitalOutput::set(bool level) { ESP_ERROR_CHECK(gpio_set_level(static_cast<gpio_num_t>(pin()), level)); }

// **************************************************************************
// * Input
// **************************************************************************

EspDigitalInput::EspDigitalInput(int pin, gpio_pullup_t pullup, gpio_pulldown_t pulldown) : _pin(pin)
{
    gpio_config_t config{
        .pin_bit_mask{pinMask()},
        .mode{GPIO_MODE_INPUT},
        .pull_up_en{pullup},
        .pull_down_en{pulldown},
        .intr_type{GPIO_INTR_DISABLE},
    };
    ESP_ERROR_CHECK(gpio_config(&config));
}

bool EspDigitalInput::get() const { return gpio_get_level(static_cast<gpio_num_t>(pin())) != 0; }

// **************************************************************************
// * Input/Output
// **************************************************************************

EspDigitalInputOutput::EspDigitalInputOutput(int pin, gpio_pullup_t pullup, gpio_pulldown_t pulldown, bool openDrain)
    : _pin(pin), _od(openDrain)
{
    gpio_config_t config{
        .pin_bit_mask{pinMask()},
        .mode{openDrain ? GPIO_MODE_INPUT_OUTPUT_OD : GPIO_MODE_INPUT},
        .pull_up_en{pullup},
        .pull_down_en{pulldown},
        .intr_type{GPIO_INTR_DISABLE},
    };
    ESP_ERROR_CHECK(gpio_config(&config));
}

bool EspDigitalInputOutput::get() const { return gpio_get_level(static_cast<gpio_num_t>(pin())) != 0; }
void EspDigitalInputOutput::set(bool level) { ESP_ERROR_CHECK(gpio_set_level(static_cast<gpio_num_t>(pin()), level)); }

void EspDigitalInputOutput::setDirection(Direction dir) const
{
    switch(dir)
    {
        case Direction::Input:
            ESP_ERROR_CHECK(
                gpio_set_direction(static_cast<gpio_num_t>(pin()), _od ? GPIO_MODE_INPUT_OUTPUT_OD : GPIO_MODE_INPUT));
            break;
        case Direction::Output:
            ESP_ERROR_CHECK(gpio_set_direction(static_cast<gpio_num_t>(pin()),
                                               _od ? GPIO_MODE_INPUT_OUTPUT_OD : GPIO_MODE_INPUT_OUTPUT));
            break;
        default:
            assert(false);  // The direction should be a valid value.
    }
}
