#pragma once

#include <esp_adc/adc_cali.h>
#include <esp_adc/adc_oneshot.h>
#include <etl/vector.h>

class EspAdc;

class EspAdcChannel
{
public:
    EspAdcChannel(EspAdc& adc, adc_channel_t channel);
    ~EspAdcChannel() = default;
    EspAdcChannel(const EspAdcChannel& other) = default;
    EspAdcChannel& operator=(const EspAdcChannel& other) = default;
    EspAdcChannel(EspAdcChannel&&) = default;
    EspAdcChannel& operator=(EspAdcChannel&&) = default;

    int getMv() const;

    adc_channel_t channel() const { return _channel; }

private:
    EspAdc& _adc;
    adc_channel_t _channel;
};

class EspAdc
{
    static constexpr const char* kTag = "ADC";

public:
    EspAdc(adc_unit_t unit = ADC_UNIT_1);
    ~EspAdc();
    EspAdc(const EspAdc& other) = delete;
    EspAdc& operator=(const EspAdc& other) = delete;
    EspAdc(EspAdc&&) = delete;
    EspAdc& operator=(EspAdc&&) = delete;

    EspAdcChannel addChannel(int pin);

    EspAdcChannel getChannel(adc_channel_t channel);

    int getMv(adc_channel_t channel) const;
    int getRaw(adc_channel_t channel) const;

private:
    adc_oneshot_unit_handle_t _adc{nullptr};
    adc_unit_t _unit;
    adc_cali_handle_t _cali{nullptr};
    adc_cali_scheme_ver_t _caliScheme{static_cast<adc_cali_scheme_ver_t>(0)};

    void initCalibration();
};
