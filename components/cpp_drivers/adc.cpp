
#include <esp_adc/adc_cali_scheme.h>
#include <esp_log.h>

#include <adc.hpp>
#include <algorithm>

EspAdc::EspAdc(adc_unit_t unit) : _unit(unit)
{
    adc_oneshot_unit_init_cfg_t config{
        .unit_id{_unit},
        .ulp_mode{ADC_ULP_MODE_DISABLE},
    };

    ESP_ERROR_CHECK(adc_oneshot_new_unit(&config, &_adc));

    initCalibration();
}

EspAdc::~EspAdc()
{
    ESP_ERROR_CHECK(adc_oneshot_del_unit(_adc));
    if(_cali)
    {
        switch(_caliScheme)
        {
#if ADC_CALI_SCHEME_LINE_FITTING_SUPPORTED
            case ADC_CALI_SCHEME_VER_LINE_FITTING:
                ESP_ERROR_CHECK(adc_cali_delete_scheme_line_fitting(_cali));
                break;
#endif
#if ADC_CALI_SCHEME_CURVE_FITTING_SUPPORTED
            case ADC_CALI_SCHEME_VER_CURVE_FITTING:
                ESP_ERROR_CHECK(adc_cali_delete_scheme_curve_fitting(_cali));
                break;
#endif
            default:
                ESP_LOGE(kTag, "Unsupported calibration scheme set %d", _caliScheme);
                break;
        }
    }
}

EspAdcChannel EspAdc::addChannel(int pin)
{
    adc_unit_t unit;
    adc_channel_t channel;
    ESP_ERROR_CHECK(adc_oneshot_io_to_channel(pin, &unit, &channel));
    assert(unit == _unit);  // The channel must belong to this ADC

    adc_oneshot_chan_cfg_t config{
        .atten{ADC_ATTEN_DB_11},
        .bitwidth{ADC_BITWIDTH_DEFAULT},
    };

    ESP_ERROR_CHECK(adc_oneshot_config_channel(_adc, channel, &config));

    return EspAdcChannel(*this, channel);
}

EspAdcChannel EspAdc::getChannel(adc_channel_t channel) { return EspAdcChannel(*this, channel); }

int EspAdc::getRaw(adc_channel_t channel) const
{
    int raw;
    ESP_ERROR_CHECK(adc_oneshot_read(_adc, channel, &raw));
    return raw;
}

int EspAdc::getMv(adc_channel_t channel) const
{
    int voltage{0};
    ESP_ERROR_CHECK(adc_cali_raw_to_voltage(_cali, getRaw(channel), &voltage));
    return voltage;
}

void EspAdc::initCalibration()
{
    adc_cali_scheme_ver_t supportedSchemes;
    ESP_ERROR_CHECK(adc_cali_check_scheme(&supportedSchemes));
    if(supportedSchemes & ADC_CALI_SCHEME_VER_CURVE_FITTING)
    {
#if ADC_CALI_SCHEME_CURVE_FITTING_SUPPORTED
        adc_cali_curve_fitting_config_t config{
            .unit_id = _unit,
            .atten = ADC_ATTEN_DB_11,
            .bitwidth = ADC_BITWIDTH_DEFAULT,
        };
        ESP_ERROR_CHECK(adc_cali_create_scheme_curve_fitting(&config, &_cali));
        _caliScheme = ADC_CALI_SCHEME_VER_CURVE_FITTING;
#endif
    }
    else if(supportedSchemes & ADC_CALI_SCHEME_VER_LINE_FITTING)
    {
#if ADC_CALI_SCHEME_LINE_FITTING_SUPPORTED
        adc_cali_line_fitting_config_t config{
            .unit_id = _unit,
            .atten = ADC_ATTEN_DB_11,
            .bitwidth = ADC_BITWIDTH_DEFAULT,
#ifdef CONFIG_IDF_TARGET_ESP32
            .default_vref = 1100,
#endif
        };
        ESP_ERROR_CHECK(adc_cali_create_scheme_line_fitting(&config, &_cali));
        _caliScheme = ADC_CALI_SCHEME_VER_LINE_FITTING;
#endif
    }

    if(_caliScheme == static_cast<adc_cali_scheme_ver_t>(0))
    {
        ESP_LOGE(kTag, "No supported calibration scheme");
    }
}

EspAdcChannel::EspAdcChannel(EspAdc& adc, adc_channel_t channel) : _adc(adc), _channel(channel) {}

int EspAdcChannel::getMv() const { return _adc.getMv(_channel); }
