#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <pthread.h>

#include <pattern_player.hpp>

DigitalPatternPlayer::DigitalPatternPlayer(IDigitalOutput& output)
    : _output(output), _thread(&DigitalPatternPlayer::task, this)
{
}

void DigitalPatternPlayer::start()
{
    assert(!_playing);
    if(_pattern.size() == 0)
    {
        ESP_LOGW(kTag, "Tried to start playing an empty pattern");
        return;
    }

    {
        std::lock_guard guard(_playing_mutex);
        _play_position = 0;
        _playing = true;
    }
    _playing_condition_variable.notify_one();
}

void DigitalPatternPlayer::stop()
{
    {
        std::lock_guard guard(_playing_mutex);
        _playing = false;
    }
    _playing_condition_variable.notify_one();
}

PatternStep DigitalPatternPlayer::getNextStep()
{
    if(_playing)
    {
        assert(_play_position < _pattern.size());
        auto step = _pattern.at(_play_position);
        _play_position = (_play_position + 1) % _pattern.size();
        return step;
    }
    else
    {
        return PatternStep{PatternStep::kStop, 0};
    }
}

void DigitalPatternPlayer::task()
{
    while(true)
    {
        std::unique_lock lock(_playing_mutex);
        _playing_condition_variable.wait(lock, [this] { return _playing; });
        auto step = getNextStep();
        lock.unlock();

        ESP_LOGD(kTag, "Step %d - %d %d", _play_position == 0 ? _pattern.size() - 1 : _play_position - 1, step.command,
                 step.argument);

        switch(step.command)
        {
            case PatternStep::kSleep:
                vTaskDelay(pdMS_TO_TICKS(step.argument * PatternStep::kMsPerVal));
                break;
            case PatternStep::kSetOutput:
                _output.set(step.argument != 0);
                break;
            case PatternStep::kGoto:
                if(step.argument >= _pattern.size())
                {
                    ESP_LOGE(kTag, "Pattern contains an invalid goto instruction to %d", step.argument);
                    stop();
                }
                else
                {
                    _play_position = step.argument;
                }
                break;
            case PatternStep::kStop:
                stop();
                break;
        }
    }
}
