#include <motor_driver.hpp>

MotorDriverGroup::MotorDriverGroup(int group, uint32_t frequency) : _group(group)
{
    mcpwm_timer_config_t config{
        .group_id{group},
        .clk_src{MCPWM_TIMER_CLK_SRC_PLL160M},
        .resolution_hz{frequency * MotorDriver::kMaxSpeed},
        .count_mode{MCPWM_TIMER_COUNT_MODE_UP},
        .period_ticks{MotorDriver::kMaxSpeed},
        .flags{
            .update_period_on_empty{1},
            .update_period_on_sync{0},
        },
    };
    ESP_ERROR_CHECK(mcpwm_new_timer(&config, &_timer));

    ESP_ERROR_CHECK(mcpwm_timer_enable(_timer));
}

MotorDriverGroup::~MotorDriverGroup()
{
    _drivers.clear();
    ESP_ERROR_CHECK(mcpwm_timer_disable(_timer));
    ESP_ERROR_CHECK(mcpwm_del_timer(_timer));
}

void MotorDriverGroup::addDriver(int pinA, int pinB) { _drivers.emplace_back(_timer, _group, pinA, pinB); }

void MotorDriverGroup::start() { ESP_ERROR_CHECK(mcpwm_timer_start_stop(_timer, MCPWM_TIMER_START_NO_STOP)); }

void MotorDriverGroup::stop() { ESP_ERROR_CHECK(mcpwm_timer_start_stop(_timer, MCPWM_TIMER_START_STOP_FULL)); }

MotorDriver::MotorDriver(mcpwm_timer_handle_t timer, int group, int pinA, int pinB)
{
    mcpwm_operator_config_t oper_config{
        .group_id{group},
        .flags{
            .update_gen_action_on_tez{0},
            .update_gen_action_on_tep{1},
            .update_gen_action_on_sync{0},
            .update_dead_time_on_tez{0},
            .update_dead_time_on_tep{1},
            .update_dead_time_on_sync{0},
        },
    };
    ESP_ERROR_CHECK(mcpwm_new_operator(&oper_config, &_operator));
    ESP_ERROR_CHECK(mcpwm_operator_connect_timer(_operator, timer));

    _outputs[0].init(*this, pinA);
    _outputs[1].init(*this, pinB);
}

MotorDriver::MotorDriver(MotorDriver &&other) : _operator(other._operator), _outputs(std::move(other._outputs))
{
    other._operator = nullptr;
}

MotorDriver::~MotorDriver()
{
    if(_operator)
    {
        ESP_ERROR_CHECK(mcpwm_del_operator(_operator));
    }
}

void MotorDriver::setSpeed(int16_t speed)
{
    speed = std::clamp(speed, kMinSpeed, kMaxSpeed);
    if(speed > 0)
    {
        // In the forward direction we will PWM input 1 of the H-bridge and set
        // input 2 to low
        _outputs[0].setSpeed(speed);
        _outputs[1].setOutput(false);
    }
    else if(speed < 0)
    {
        // In the reverse direction we will PWM input 2 of the H-bridge and set
        // input 1 to low
        _outputs[0].setOutput(false);
        _outputs[1].setSpeed(-speed);
    }
    else
    {
        brake();
    }
}

void MotorDriver::brake()
{
    // To brake we must set both input 1 and 2 to high
    _outputs[0].setOutput(true);
    _outputs[1].setOutput(true);
}

void MotorDriver::coast()
{
    // Coasting is both outputs low.
    _outputs[0].setOutput(false);
    _outputs[1].setOutput(false);
}

void MotorDriver::Output::init(MotorDriver &driver, int pin)
{
    mcpwm_comparator_config_t comp_config{
        .flags{
            .update_cmp_on_tez{0},
            .update_cmp_on_tep{1},
            .update_cmp_on_sync{0},
        },
    };
    ESP_ERROR_CHECK(mcpwm_new_comparator(driver._operator, &comp_config, &compare));

    mcpwm_generator_config_t gena_config{
        .gen_gpio_num{pin},
        .flags{
            .invert_pwm{0},
            .io_loop_back{0},
        },
    };
    ESP_ERROR_CHECK(mcpwm_new_generator(driver._operator, &gena_config, &generator));
    ESP_ERROR_CHECK(mcpwm_generator_set_action_on_compare_event(
        generator, MCPWM_GEN_COMPARE_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, compare, MCPWM_GEN_ACTION_LOW)));
    ESP_ERROR_CHECK(mcpwm_generator_set_action_on_timer_event(
        generator,
        MCPWM_GEN_TIMER_EVENT_ACTION(MCPWM_TIMER_DIRECTION_UP, MCPWM_TIMER_EVENT_EMPTY, MCPWM_GEN_ACTION_HIGH)));
}

MotorDriver::Output::~Output()
{
    if(generator)
    {
        ESP_ERROR_CHECK(mcpwm_del_generator(generator));
    }
    if(compare)
    {
        ESP_ERROR_CHECK(mcpwm_del_comparator(compare));
    }
}

MotorDriver::Output::Output(Output &&other) : compare(other.compare), generator(other.generator)
{
    other.generator = nullptr;
    other.compare = nullptr;
}

void MotorDriver::Output::setSpeed(int16_t speed)
{
    assert(speed >= 0 && speed <= kMaxSpeed);
    enablePwm(true);
    ESP_ERROR_CHECK(mcpwm_comparator_set_compare_value(compare, speed));
}

void MotorDriver::Output::enablePwm(bool enable)
{
    ESP_ERROR_CHECK(mcpwm_generator_set_force_level(generator, -1, true));
}

void MotorDriver::Output::setOutput(bool level)
{
    ESP_ERROR_CHECK(mcpwm_generator_set_force_level(generator, level ? 1 : 0, true));
}
