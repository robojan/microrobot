#pragma once

#include <driver/mcpwm_cmpr.h>
#include <driver/mcpwm_gen.h>
#include <driver/mcpwm_oper.h>
#include <driver/mcpwm_timer.h>

#include <array>
#include <cassert>
#include <vector>

class MotorDriver
{
public:
    static constexpr int16_t kMaxSpeed = 1024;
    static constexpr int16_t kMinSpeed = -1024;
    static_assert(kMaxSpeed == -kMinSpeed);

    MotorDriver(mcpwm_timer_handle_t timer, int group, int pinA, int pinB);
    ~MotorDriver();
    MotorDriver(const MotorDriver &) = delete;
    MotorDriver(MotorDriver &&other);
    MotorDriver &operator=(const MotorDriver &) = delete;

    void setSpeed(int16_t speed);
    void brake();
    void coast();

private:
    struct Output
    {
        mcpwm_cmpr_handle_t compare{nullptr};
        mcpwm_gen_handle_t generator{nullptr};

        Output() = default;
        Output(Output &&other);
        ~Output();

        void init(MotorDriver &driver, int pin);

        void setSpeed(int16_t speed);
        void enablePwm(bool enable);
        void setOutput(bool level);
    };

    mcpwm_oper_handle_t _operator;
    std::array<Output, 2> _outputs;
};

class MotorDriverGroup
{
public:
    MotorDriverGroup(int group, uint32_t frequency);
    ~MotorDriverGroup();
    MotorDriverGroup(const MotorDriverGroup &) = delete;
    MotorDriverGroup &operator=(const MotorDriverGroup &) = delete;

    void addDriver(int pinA, int pinB);

    MotorDriver &driver(int idx)
    {
        assert(idx >= 0 && idx < _drivers.size());
        return _drivers[idx];
    };

    void start();
    void stop();

private:
    mcpwm_timer_handle_t _timer;
    std::vector<MotorDriver> _drivers;
    int _group;
};
