#pragma once

#include <etl/vector.h>

#include <algorithm>
#include <atomic>
#include <cassert>
#include <condition_variable>
#include <cstdint>
#include <freertos/mutex.hpp>
#include <igpio.hpp>
#include <mutex>
#include <span>
#include <string_view>
#include <thread>
#include <type_traits>

struct PatternStep
{
    static constexpr unsigned kMsPerVal = 10;
    enum Command
    {
        kSleep,
        kSetOutput,
        kGoto,
        kStop,
    };

    Command command : 4;
    uint16_t argument : 12;
};

template <size_t kMaxSize = 10, size_t kMaxLabels = 2>
class PatternBuilder
{
public:
    constexpr PatternBuilder() = default;
    constexpr ~PatternBuilder() = default;
    constexpr PatternBuilder(const PatternBuilder& other) = default;
    constexpr PatternBuilder& operator=(const PatternBuilder& other) = default;
    constexpr PatternBuilder(PatternBuilder&& other) = delete;
    constexpr PatternBuilder& operator=(PatternBuilder&& other) = delete;

    constexpr PatternBuilder& label(std::string_view name)
    {
        pushLabel(name, _num_steps);
        return *this;
    }

    constexpr PatternBuilder& addSleep(unsigned int sleepMs)
    {
        pushStep(PatternStep{
            .command{PatternStep::kSleep},
            // Round up the sleep time
            .argument{static_cast<uint16_t>((sleepMs + PatternStep::kMsPerVal - 1) / PatternStep::kMsPerVal)},
        });
        return *this;
    }

    constexpr PatternBuilder& addSetOutput(bool level)
    {
        pushStep(PatternStep{
            .command{PatternStep::kSetOutput},
            .argument{static_cast<uint16_t>(level)},
        });
        return *this;
    }

    constexpr PatternBuilder& addGoto(std::string_view label)
    {
        auto it = std::find_if(_labels.begin(), _labels.end(), [label](const auto& p) { return p.first == label; });
        assert(it != _labels.end());
        return addGoto(it->second);
    }

    constexpr PatternBuilder& addGoto(size_t idx)
    {
        assert(idx < kMaxSize);
        pushStep(PatternStep{
            .command{PatternStep::kGoto},
            .argument{static_cast<uint16_t>(idx)},
        });
        return *this;
    }

    constexpr PatternBuilder& addStop()
    {
        pushStep(PatternStep{
            .command{PatternStep::kStop},
            .argument{0},
        });
        return *this;
    }

    auto build()
    {
        // FIXME: Make this function constexpr to have a full constexpr builder
        // if(std::is_constant_evaluated) std::array<PatternStep, _num_steps> result;
        // std::copy(_steps.begin(), _steps.begin() + _num_steps, result.begin());
        // return result;
        // }
        // else
        // {
        return std::vector<PatternStep>(_steps.begin(), _steps.begin() + _num_steps);
        // }
    }

private:
    std::array<PatternStep, kMaxSize> _steps;
    size_t _num_steps{0};
    std::array<std::pair<std::string_view, size_t>, kMaxLabels> _labels;
    size_t _num_labels{0};

    constexpr void pushStep(const PatternStep& step)
    {
        assert(_num_steps < kMaxSize);
        _steps[_num_steps] = step;
        _num_steps++;
    }
    constexpr void pushLabel(const std::string_view& label, size_t idx)
    {
        assert(_num_labels < kMaxLabels);
        _labels[_num_labels] = std::make_pair(label, idx);
        _num_labels++;
    }
};

class DigitalPatternPlayer final
{
    static constexpr const char* kTag = "DigitalPatternPlayer";

public:
    DigitalPatternPlayer(IDigitalOutput& output);
    ~DigitalPatternPlayer() = default;
    DigitalPatternPlayer(const DigitalPatternPlayer& copyFrom) = delete;
    DigitalPatternPlayer& operator=(const DigitalPatternPlayer& copyFrom) = delete;
    DigitalPatternPlayer(DigitalPatternPlayer&&) = delete;
    DigitalPatternPlayer& operator=(DigitalPatternPlayer&&) = delete;

    template <typename Container>
    void load(const Container& c)
    {
        load(c.begin(), c.end());
    }

    template <typename InputIt>
    void load(InputIt begin, InputIt end)
    {
        assert(!_playing);
        _pattern.assign(begin, end);
    }

    void start();
    void stop();

private:
    IDigitalOutput& _output;
    std::vector<PatternStep> _pattern;
    size_t _play_position{0};
    bool _playing{false};
    std::mutex _playing_mutex;
    std::jthread _thread;
    std::condition_variable _playing_condition_variable;

    PatternStep getNextStep();

    void task();
};
