#pragma once

class IDigitalOutput
{
public:
    virtual ~IDigitalOutput() = default;
    IDigitalOutput() = default;
    IDigitalOutput(const IDigitalOutput& copyFrom) = default;
    IDigitalOutput& operator=(const IDigitalOutput& copyFrom) = default;
    IDigitalOutput(IDigitalOutput&&) = default;
    IDigitalOutput& operator=(IDigitalOutput&&) = default;

    virtual void set(bool value) = 0;
};

class IDigitalInput
{
public:
    virtual ~IDigitalInput() = default;
    IDigitalInput() = default;
    IDigitalInput(const IDigitalInput& copyFrom) = default;
    IDigitalInput& operator=(const IDigitalInput& copyFrom) = default;
    IDigitalInput(IDigitalInput&&) = default;
    IDigitalInput& operator=(IDigitalInput&&) = default;

    virtual bool get() const = 0;
};

class IDigitalInputOutput : public IDigitalInput, public IDigitalOutput
{
public:
    enum Direction
    {
        Input,
        Output
    };

    virtual ~IDigitalInputOutput() = default;
    IDigitalInputOutput() = default;
    IDigitalInputOutput(const IDigitalInputOutput& copyFrom) = default;
    IDigitalInputOutput& operator=(const IDigitalInputOutput& copyFrom) = default;
    IDigitalInputOutput(IDigitalInputOutput&&) = default;
    IDigitalInputOutput& operator=(IDigitalInputOutput&&) = default;

    virtual void setDirection(Direction dir) const = 0;
};
