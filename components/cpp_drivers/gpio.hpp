#pragma once

#include <driver/gpio.h>
#include <stdint.h>

#include <igpio.hpp>

class EspDigitalOutput final : public IDigitalOutput
{
public:
    EspDigitalOutput(int pin, bool defaultLevel);
    EspDigitalOutput(int pin, bool defaultLevel, gpio_pullup_t pullup, gpio_pulldown_t pulldown, bool openDrain = true);
    virtual ~EspDigitalOutput() = default;
    EspDigitalOutput(const EspDigitalOutput& copyFrom) = delete;
    EspDigitalOutput& operator=(const EspDigitalOutput& copyFrom) = delete;
    EspDigitalOutput(EspDigitalOutput&&) = delete;
    EspDigitalOutput& operator=(EspDigitalOutput&&) = delete;

    void set(bool level) override;

private:
    int _pin;

    int pin() const noexcept { return _pin; }
    uint64_t pinMask() const noexcept { return uint64_t{1} << pin(); }
};

class EspDigitalInput final : public IDigitalInput
{
public:
    EspDigitalInput(int pin, gpio_pullup_t pullup = GPIO_PULLUP_DISABLE,
                    gpio_pulldown_t pulldown = GPIO_PULLDOWN_DISABLE);
    virtual ~EspDigitalInput() = default;
    EspDigitalInput(const EspDigitalInput& copyFrom) = delete;
    EspDigitalInput& operator=(const EspDigitalInput& copyFrom) = delete;
    EspDigitalInput(EspDigitalInput&&) = delete;
    EspDigitalInput& operator=(EspDigitalInput&&) = delete;

    bool get() const override;

private:
    int _pin;

    int pin() const noexcept { return _pin; }
    uint64_t pinMask() const noexcept { return uint64_t{1} << pin(); }
};

class EspDigitalInputOutput final : public IDigitalInputOutput
{
public:
    EspDigitalInputOutput(int pin, gpio_pullup_t pullup = GPIO_PULLUP_DISABLE,
                          gpio_pulldown_t pulldown = GPIO_PULLDOWN_DISABLE, bool openDrain = false);
    virtual ~EspDigitalInputOutput() = default;
    EspDigitalInputOutput(const EspDigitalInputOutput& copyFrom) = delete;
    EspDigitalInputOutput& operator=(const EspDigitalInputOutput& copyFrom) = delete;
    EspDigitalInputOutput(EspDigitalInputOutput&&) = delete;
    EspDigitalInputOutput& operator=(EspDigitalInputOutput&&) = delete;

    bool get() const override;
    void set(bool level) override;
    void setDirection(Direction dir) const override;

private:
    int _pin;
    bool _od;

    int pin() const noexcept { return _pin; }
    uint64_t pinMask() const noexcept { return uint64_t{1} << pin(); }
};
