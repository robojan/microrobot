#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <adc.hpp>
#include <bluepad_platform.hpp>
#include <motorset.hpp>

class LineFollower
{
    static constexpr unsigned kModelPeriod = 10;
    static constexpr const char *kTag = "LineFollower";

public:
    LineFollower(const MotorSet &motors, const EspAdcChannel &leftSensor, const EspAdcChannel &rightSensor);

    void start();
    void tick();
    void stop();

private:
    TickType_t _lastRunTime{0};

    MotorSet _motors;
    const EspAdcChannel &_leftSensor;
    const EspAdcChannel &_rightSensor;
};
