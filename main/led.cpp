#include <led.hpp>

Led::Led(IDigitalOutput& output) : _output(output), _player(output) {}

void Led::set(bool level)
{
    _player.stop();
    _output.set(kInverted ? !level : level);
}

void Led::blink(uint32_t periodMs)
{
    // -_
    _player.stop();
    _player.load(PatternBuilder()
                     .addSetOutput(kEnableValue)
                     .addSleep(periodMs / 2)  // round down
                     .addSetOutput(kDisableValue)
                     .addSleep((periodMs + 1) / 2)  // round up
                     .build());
    _player.start();
}

void Led::heartbeat(uint32_t periodMs)
{
    // -_-___
    _player.stop();
    _player.load(PatternBuilder()
                     .addSetOutput(kEnableValue)
                     .addSleep(periodMs / 6)  // round down
                     .addSetOutput(kDisableValue)
                     .addSleep((periodMs + 5) / 6)  // round up
                     .addSetOutput(kEnableValue)
                     .addSleep(periodMs / 6)  // round down
                     .addSetOutput(kDisableValue)
                     .addSleep(periodMs / 2)
                     .build());
    _player.start();
}

void Led::pulse(int numPulses)
{
    auto builder = PatternBuilder<40>();
    for(int i = 0; i < numPulses; i++)
    {
        builder.addSetOutput(kEnableValue).addSleep(250).addSetOutput(kDisableValue).addSleep(250);
    }
    builder.addSleep(1000);

    _player.stop();
    _player.load(builder.build());
    _player.start();
}
