
#include <cmd_nvs.h>
#include <cmd_system.h>
#include <cmd_wifi.h>
#include <esp_console.h>
#include <esp_log.h>
#include <nvs.h>
#include <nvs_flash.h>
#include <stdio.h>
#include <uni_console.h>

#include <adc.hpp>
#include <bluepad_platform.hpp>
#include <gpio.hpp>
#include <hardware.hpp>
#include <led.hpp>
#include <line_follower.hpp>
#include <motor_driver.hpp>
#include <optional>
#include <remote_control.hpp>

class MainApplication
{
    static constexpr const char* TAG = "main";

public:
    void init()
    {
        initLed();
        initNvs();
        initConsole();
        initMotors();
        initRemoteControl();
        initLineFollower();

        _bluepad.startService();
        _bluepad.setAllowPairing(true);
    }

    [[noreturn]] void run()
    {
        ESP_LOGI(TAG, "starting");

        // Start processing the console
        ESP_ERROR_CHECK(esp_console_start_repl(_repl));

        _motorGroupA.start();
        _motorGroupB.start();

        TickType_t lastPrintTime = xTaskGetTickCount();

        while(true)
        {
            auto& controller = _bluepad.getControllerState();
            if(controller.buttonA())
            {
                enterState(State::kRemoteControl);
            }
            else if(controller.buttonY())
            {
                enterState(State::kLineFollowing);
            }
            switch(_state)
            {
                case State::kWaitingForController:
                    processWaitForController();
                    break;
                case State::kRemoteControl:
                    processRemoteControl();
                    break;
                case State::kLineFollowing:
                    processLineFollowing();
                    break;
            }
            if(xTaskGetTickCount() - lastPrintTime > pdMS_TO_TICKS(500))
            {
                // Print the sensor values
                ESP_LOGI(TAG, "sense1: %d, sense2: %d", _senseAdc1.getMv(), _senseAdc2.getMv());
                lastPrintTime = xTaskGetTickCount();
            }
        }

        vTaskDelete(nullptr);
    }

private:
    enum class State
    {
        kWaitingForController,
        kRemoteControl,
        kLineFollowing,
    };

    esp_console_repl_t* _repl = nullptr;
    BluepadPlatform _bluepad;
    // 39062.5 Hz will give a clean 1024 division from 160MHz prescaled by 4.
    MotorDriverGroup _motorGroupA{0, 39062};
    MotorDriverGroup _motorGroupB{1, 39062};
    EspDigitalOutput _motorNSleep{hardware::pins::kNMotorSleep, false};
    EspDigitalOutput _ledOutput{hardware::pins::kNLed, true};
    Led _led{_ledOutput};
    EspAdc _senseAdc;
    EspAdcChannel _senseAdc1{_senseAdc.addChannel(hardware::pins::kRefSense1)};
    EspAdcChannel _senseAdc2{_senseAdc.addChannel(hardware::pins::kRefSense2)};
    State _state{State::kWaitingForController};
    std::optional<MotorSet> _motorSet;
    std::optional<RemoteControl> _remoteControl;
    std::optional<LineFollower> _lineFollower;

    void initLed() { _led.blink(250); }

    void initConsole()
    {
        esp_console_repl_config_t repl_config = ESP_CONSOLE_REPL_CONFIG_DEFAULT();
        repl_config.prompt = "micro-robot>";
        repl_config.max_cmdline_length = 1024;

        esp_console_dev_uart_config_t hw_config = ESP_CONSOLE_DEV_UART_CONFIG_DEFAULT();
        ESP_ERROR_CHECK(esp_console_new_repl_uart(&hw_config, &repl_config, &_repl));

        ESP_ERROR_CHECK(esp_console_register_help_command());
        register_system_common();
        register_wifi();
        register_nvs();
        register_bluepad32();
    }

    void initNvs()
    {
        esp_err_t err = nvs_flash_init();
        if(err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND)
        {
            ESP_ERROR_CHECK(nvs_flash_erase());
            err = nvs_flash_init();
        }
        ESP_ERROR_CHECK(err);
    }

    void initMotors()
    {
        _motorGroupA.addDriver(hardware::pins::kMotor1A, hardware::pins::kMotor1B);
        _motorGroupA.addDriver(hardware::pins::kMotor2A, hardware::pins::kMotor2B);
        _motorGroupB.addDriver(hardware::pins::kMotor3A, hardware::pins::kMotor3B);
        _motorGroupB.addDriver(hardware::pins::kMotor4A, hardware::pins::kMotor4B);

        _motorSet.emplace(
            MotorSet{_motorGroupA.driver(1), _motorGroupB.driver(1), _motorGroupA.driver(0), _motorGroupB.driver(0)});
    }

    void initRemoteControl() { _remoteControl.emplace(_motorSet.value(), _bluepad.getControllerState()); }
    void initLineFollower() { _lineFollower.emplace(_motorSet.value(), _senseAdc1, _senseAdc2); }

    void disableMotors()
    {
        _motorGroupA.driver(0).brake();
        _motorGroupA.driver(1).brake();
        _motorGroupB.driver(0).brake();
        _motorGroupB.driver(1).brake();
    }

    void exitState()
    {
        switch(_state)
        {
            case State::kWaitingForController:
                _motorNSleep.set(true);
                break;
            case State::kRemoteControl:
                _remoteControl->stop();
                break;
            case State::kLineFollowing:
                _lineFollower->stop();
                break;
        }
    }

    void enterState(State state)
    {
        if(state == _state) return;
        exitState();
        switch(state)
        {
            case State::kWaitingForController:
                _led.blink(250);
                disableMotors();
                _motorNSleep.set(false);
                break;
            case State::kRemoteControl:
                ESP_LOGI(TAG, "Starting remote control");
                _remoteControl->start();
                break;
            case State::kLineFollowing:
                ESP_LOGI(TAG, "Starting line following");
                _lineFollower->start();
                break;
        }
        _state = state;
    }

    void processWaitForController()
    {
        if(_bluepad.isControllerConnected())
        {
            ESP_LOGI(TAG, "Controller connected");
            enterState(State::kRemoteControl);
        }
        else
        {
            vTaskDelay(pdMS_TO_TICKS(10));
        }
    }

    void processRemoteControl()
    {
        if(!_bluepad.isControllerConnected())
        {
            ESP_LOGI(TAG, "Controller disconnected");
            enterState(State::kWaitingForController);
        }
        else
        {
            _remoteControl->tick();
        }
    }

    void processLineFollowing()
    {
        if(!_bluepad.isControllerConnected())
        {
            ESP_LOGI(TAG, "Controller disconnected");
            enterState(State::kWaitingForController);
        }
        else
        {
            _lineFollower->tick();
        }
    }
};

static MainApplication g_main;

extern "C" void app_main(void)
{
    g_main.init();

    g_main.run();
}
