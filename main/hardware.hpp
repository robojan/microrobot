#pragma once

#include <sdkconfig.h>

namespace hardware
{
#if defined(CONFIG_MICROROBOT_PLATFORM_MICROROBOT)
namespace pins
{
// Motor driver outputs
constexpr int kMotor1A = 19;
constexpr int kMotor1B = 22;
constexpr int kMotor2A = 8;
constexpr int kMotor2B = 5;
constexpr int kMotor3A = 10;
constexpr int kMotor3B = 7;
constexpr int kMotor4A = 9;
constexpr int kMotor4B = 20;
constexpr int kNMotorSleep = 21;

// Debug UART
constexpr int kDebugTx = 1;
constexpr int kDebugRx = 3;

// Reflection sensors
constexpr int kRefSense1 = 33;
constexpr int kRefSense2 = 32;

// I2C bus
constexpr int kSda = 25;
constexpr int kScl = 26;

// IMU
constexpr int kImuIrq = 27;

// LED
constexpr int kNLed = 4;

// JTAG
constexpr int kTms = 14;
constexpr int kTdi = 12;
constexpr int kTck = 13;
constexpr int kTdo = 15;
}  // namespace pins
#elif defined(CONFIG_MICROROBOT_PLATFORM_WROOM32)
namespace pins
{
// Motor driver outputs
constexpr int kMotor1A = 19;
constexpr int kMotor1B = 22;
constexpr int kMotor2A = 18;
constexpr int kMotor2B = 5;
constexpr int kMotor3A = 16;
constexpr int kMotor3B = 17;
constexpr int kMotor4A = 23;
constexpr int kMotor4B = 2;
constexpr int kNMotorSleep = 21;

// Debug UART
constexpr int kDebugTx = 1;
constexpr int kDebugRx = 3;

// Reflection sensors
constexpr int kRefSense1 = 33;
constexpr int kRefSense2 = 32;

// I2C bus
constexpr int kSda = 25;
constexpr int kScl = 26;

// IMU
constexpr int kImuIrq = 27;

// LED
constexpr int kNLed = 4;

// JTAG
constexpr int kTms = 14;
constexpr int kTdi = 12;
constexpr int kTck = 13;
constexpr int kTdo = 15;
}  // namespace pins

#else
#error You need to select an supported platform
#endif
}  // namespace hardware
