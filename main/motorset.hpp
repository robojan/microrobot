#pragma once

#include <motor_driver.hpp>

struct MotorSet
{
    MotorDriver &leftFront;
    MotorDriver &leftBack;
    MotorDriver &rightFront;
    MotorDriver &rightBack;

    void brake()
    {
        leftFront.brake();
        rightFront.brake();
        leftBack.brake();
        rightBack.brake();
    }

    void goInDirection(float x, float y);
};
