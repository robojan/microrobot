#include <esp_log.h>

#include <adc.hpp>
#include <bluepad_platform.hpp>
#include <motor_driver.hpp>
#include <remote_control.hpp>

RemoteControl::RemoteControl(const MotorSet &motors, const ControllerState &controller)
    : _motors(motors), _controller(controller)
{
}

void RemoteControl::start() { _lastRunTime = xTaskGetTickCount(); }

void RemoteControl::tick()
{
    // Wait until next tick
    vTaskDelayUntil(&_lastRunTime, pdMS_TO_TICKS(kModelPeriod));

    // Sense
    auto stickX = _controller.leftStickX();
    auto stickY = -_controller.leftStickY();  // The Y stick direction is swapped

    static constexpr int kDeadZone = ControllerState::kMaxStickAmplitude / 10;
    if(abs(stickX) < kDeadZone && abs(stickY) < kDeadZone)
    {
        // Dead zone
        stickX = 0;
        stickY = 0;
    }

    _motors.goInDirection(static_cast<float>(stickX) / ControllerState::kMaxStickAmplitude,
                          static_cast<float>(stickY) / ControllerState::kMaxStickAmplitude);
}

void RemoteControl::stop() { _motors.brake(); }
