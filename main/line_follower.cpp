#include <esp_log.h>

#include <adc.hpp>
#include <bluepad_platform.hpp>
#include <line_follower.hpp>
#include <motor_driver.hpp>

LineFollower::LineFollower(const MotorSet &motors, const EspAdcChannel &leftSensor, const EspAdcChannel &rightSensor)
    : _motors(motors), _leftSensor(leftSensor), _rightSensor(rightSensor)
{
}

void LineFollower::start() { _lastRunTime = xTaskGetTickCount(); }

void LineFollower::tick()
{
    // Wait until next tick
    vTaskDelayUntil(&_lastRunTime, pdMS_TO_TICKS(kModelPeriod));

    // Sense
    auto leftSensorValue = _leftSensor.getMv();
    auto rightSensorValue = _rightSensor.getMv();

    static constexpr int kSensorThreshold = 2800;

    // Compute
    auto leftSensorOnLine = leftSensorValue > kSensorThreshold;
    auto rightSensorOnLine = rightSensorValue > kSensorThreshold;

    ESP_LOGI(kTag, "Left sensor: %d, right sensor: %d", leftSensorOnLine, rightSensorOnLine);

    // Actuate
    static constexpr float kForwardSpeed = 0.17f;
    static constexpr float kTurningSpeed = 0.17f;
    static constexpr float kBackwardSpeed = 0.17f;
    if(leftSensorOnLine && rightSensorOnLine)
    {
        // Both sensors on line
        _motors.goInDirection(0, kForwardSpeed);
    }
    else if(leftSensorOnLine)
    {
        // Only left sensor on line
        _motors.goInDirection(-kTurningSpeed, 0);
    }
    else if(rightSensorOnLine)
    {
        // Only right sensor on line
        _motors.goInDirection(kTurningSpeed, 0);
    }
    else
    {
        // Both sensors off line
        _motors.goInDirection(0, -kBackwardSpeed);
    }
}

void LineFollower::stop() { _motors.brake(); }
