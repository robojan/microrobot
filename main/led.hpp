#pragma once

#include <igpio.hpp>
#include <pattern_player.hpp>

class Led
{
    static constexpr bool kInverted = true;
    static constexpr bool kEnableValue = kInverted ? false : true;
    static constexpr bool kDisableValue = !kEnableValue;

public:
    Led(IDigitalOutput& output);
    virtual ~Led() = default;
    Led(const Led& copyFrom) = delete;
    Led& operator=(const Led& copyFrom) = delete;
    Led(Led&&) = delete;
    Led& operator=(Led&&) = delete;

    void set(bool level);
    void blink(uint32_t periodMs);
    void heartbeat(uint32_t periodMs);
    void pulse(int numPulses);

private:
    IDigitalOutput& _output;
    DigitalPatternPlayer _player;
};
