#include <btstack_port_esp32.h>
#include <esp_log.h>
#include <uni_bt_setup.h>
#include <uni_platform.h>

#include <bluepad_platform.hpp>

extern "C"
{
#include <uni_esp32.h>
#include <uni_hid_device.h>
#include <uni_main.h>

#include "uni_property.h"
#include "uni_version.h"
}

constexpr uni_platform BluepadPlatform::kBpPlatform{
    .name{const_cast<char *>("Micro Robot")},
    .init{&BluepadPlatform::s_init},
    .on_init_complete{&BluepadPlatform::s_onInitComplete},
    .on_device_connected{&BluepadPlatform::s_onDeviceConnected},
    .on_device_disconnected{&BluepadPlatform::s_onDeviceDisconnected},
    .on_device_ready{&BluepadPlatform::s_onDeviceReady},
    .on_gamepad_data{nullptr},
    .on_controller_data{&BluepadPlatform::s_onControllerData},
    .get_property{&BluepadPlatform::s_getPlatformProperty},
    .on_oob_event{&BluepadPlatform::s_onOobEvent},
    .device_dump{&BluepadPlatform::s_deviceDump},
    .register_console_cmds{&BluepadPlatform::s_registerConsoleCommands},
};
BluepadPlatform *BluepadPlatform::s_activePlatform{nullptr};

extern "C" uni_platform *uni_get_custom_platform() { return BluepadPlatform::getUniPlatform(); }

BluepadPlatform::BluepadPlatform()
{
    // Set the active platform this this object, if no other object exists
    assert(!s_activePlatform);
    s_activePlatform = this;
}

BluepadPlatform::~BluepadPlatform()
{
    // Clear the active platform
    assert(s_activePlatform);
    s_activePlatform = nullptr;
}

void BluepadPlatform::setAllowPairing(bool allow) { _allowPairing = allow; }

void BluepadPlatform::startService()
{
    xTaskCreate(
        [](void *)
        {
            // Configure BTstack for ESP32 VHCI Controller
            btstack_init();

            // uni_property_init();
            uni_platform_init(0, nullptr);
            uni_hid_device_setup();

            // Continue with bluetooth setup.
            uni_bt_setup();
            btstack_run_loop_execute();
        },
        "Bluepad", 5000, nullptr, 17, &_serviceTask);
}

// ###############################################################
// # Callbacks
// ###############################################################
void BluepadPlatform::init(int, const char **)
{  // Do nothing
}

void BluepadPlatform::onInitComplete()
{
    // Do nothing
}

void BluepadPlatform::onDeviceConnected(uni_hid_device_t *device)
{
    ESP_LOGD(kTag, "OnDeviceConnected %p %s %d", device, device->name, device->controller.klass);
}

void BluepadPlatform::onDeviceDisconnected(uni_hid_device_t *device)
{
    ESP_LOGI(kTag, "OnDeviceDisconnected %p %s", device, device->name);
    _connectedDevice = nullptr;
}

int BluepadPlatform::onDeviceReady(uni_hid_device_t *device)
{
    // This function returns 0 for true, 1 for false
    if(canControllerPair() && uni_hid_device_is_gamepad(device))
    {
        _connectedDevice = device;
        return 0;
    }
    else
    {
        return 1;
    }
}

void BluepadPlatform::onControllerData(uni_hid_device_t *device, uni_controller_t *controller)
{
    ESP_LOGD(kTag, "OnControllerData x: %ld y: %ld dpad: %02x buttons %04x brake: %ld throttle: %ld",
             controller->gamepad.axis_x, controller->gamepad.axis_y, controller->gamepad.dpad,
             controller->gamepad.buttons, controller->gamepad.brake, controller->gamepad.throttle);
    if(!isControllerConnected())
    {
        ESP_LOGE(kTag, "Received data while no controller was connected");
        return;
    }
    assert(controller->klass == UNI_CONTROLLER_CLASS_GAMEPAD);
    _controllerState.updateState(controller->gamepad);
}

int32_t BluepadPlatform::getPlatformProperty(uni_platform_property_t key)
{
    // This function is a bit strange that it returns -1 for unsupported properties, 0 for true, 1 for false.
    ESP_LOGD(kTag, "getPlatformProperty %d", key);
    switch(key)
    {
        case UNI_PLATFORM_PROPERTY_DELETE_STORED_KEYS:
            return 0;  // Delete keys on boot (true)
        default:
            return -1;  // Not supported
    }
}

void BluepadPlatform::onOobEvent(uni_platform_oob_event_t event, void *data)
{
    ESP_LOGD(kTag, "OnOobEvent %d %p", event, data);
}

void BluepadPlatform::deviceDump(uni_hid_device_t *device) { ESP_LOGI(kTag, "deviceDump"); }

void BluepadPlatform::registerConsoleCommands() { ESP_LOGI(kTag, "registerConsoleCommands"); }

// #################################################################
// # Static wrappers
// #################################################################

void BluepadPlatform::s_init(int argc, const char **argv)
{
    assert(s_activePlatform);
    s_activePlatform->init(argc, argv);
}

void BluepadPlatform::s_onInitComplete()
{
    assert(s_activePlatform);
    s_activePlatform->onInitComplete();
}

void BluepadPlatform::s_onDeviceConnected(uni_hid_device_t *device)
{
    assert(s_activePlatform);
    s_activePlatform->onDeviceConnected(device);
}

void BluepadPlatform::s_onDeviceDisconnected(uni_hid_device_t *device)
{
    assert(s_activePlatform);
    s_activePlatform->onDeviceDisconnected(device);
}

int BluepadPlatform::s_onDeviceReady(uni_hid_device_t *device)
{
    assert(s_activePlatform);
    return s_activePlatform->onDeviceReady(device);
}

void BluepadPlatform::s_onControllerData(uni_hid_device_t *device, uni_controller_t *controller)
{
    assert(s_activePlatform);
    s_activePlatform->onControllerData(device, controller);
}

int32_t BluepadPlatform::s_getPlatformProperty(uni_platform_property_t key)
{
    assert(s_activePlatform);
    return s_activePlatform->getPlatformProperty(key);
}

void BluepadPlatform::s_onOobEvent(uni_platform_oob_event_t event, void *data)
{
    assert(s_activePlatform);
    s_activePlatform->onOobEvent(event, data);
}

void BluepadPlatform::s_deviceDump(uni_hid_device_t *device)
{
    assert(s_activePlatform);
    s_activePlatform->deviceDump(device);
}

void BluepadPlatform::s_registerConsoleCommands()
{
    assert(s_activePlatform);
    s_activePlatform->registerConsoleCommands();
}
