#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <uni_controller.h>
#include <uni_platform.h>

extern "C"
{
#include <uni_hid_device.h>
}

class ControllerState
{
public:
    static constexpr int kMaxStickAmplitude = 1023;

    bool dpadLeft() const { return _state.dpad & DPAD_LEFT; }
    bool dpadUp() const { return _state.dpad & DPAD_UP; }
    bool dpadRight() const { return _state.dpad & DPAD_RIGHT; }
    bool dpadDown() const { return _state.dpad & DPAD_DOWN; }

    bool buttonA() const { return _state.buttons & BUTTON_A; }
    bool buttonB() const { return _state.buttons & BUTTON_B; }
    bool buttonX() const { return _state.buttons & BUTTON_X; }
    bool buttonY() const { return _state.buttons & BUTTON_Y; }
    bool buttonLB() const { return _state.buttons & BUTTON_SHOULDER_L; }
    bool buttonRB() const { return _state.buttons & BUTTON_SHOULDER_R; }
    bool buttonThumbLeft() const { return _state.buttons & BUTTON_THUMB_L; }
    bool buttonThumbRight() const { return _state.buttons & BUTTON_THUMB_R; }
    bool buttonXbox() const { return _state.misc_buttons & MISC_BUTTON_SYSTEM; }
    bool buttonMenu() const { return _state.misc_buttons & MISC_BUTTON_BACK; }
    bool buttonView() const { return _state.misc_buttons & MISC_BUTTON_HOME; }

    // Analog values go from -1023 - 1023
    int leftStickX() const { return _state.axis_x * 2; }
    int leftStickY() const { return _state.axis_y * 2; }
    int rightStickX() const { return _state.axis_rx * 2; }
    int rightStickY() const { return _state.axis_ry * 2; }

    // triggers go from 0 - 1023
    int leftTrigger() const { return _state.brake; }
    int rightTrigger() const { return _state.throttle; }

    void updateState(const uni_gamepad_t &state) { _state = state; }

private:
    uni_gamepad_t _state;
};

class BluepadPlatform
{
public:
    BluepadPlatform();
    BluepadPlatform(const BluepadPlatform &) = delete;
    ~BluepadPlatform();

    void startService();

    BluepadPlatform &operator=(const BluepadPlatform &) = delete;

    void setAllowPairing(bool allow);
    bool isPairingAllowed() const { return _allowPairing; }
    bool isControllerConnected() const { return _connectedDevice; }
    bool canControllerPair() const { return isPairingAllowed() && !isControllerConnected(); }

    const ControllerState &getControllerState() const { return _controllerState; }

    static uni_platform *getUniPlatform() { return const_cast<uni_platform *>(&kBpPlatform); }

private:
    static constexpr const char *kTag = "BluepadPlatform";
    static const uni_platform kBpPlatform;

    static BluepadPlatform *s_activePlatform;
    bool _allowPairing{false};
    ControllerState _controllerState;
    uni_hid_device_t *_connectedDevice{nullptr};
    TaskHandle_t _serviceTask{nullptr};

    static void s_init(int argc, const char **argv);
    static void s_onInitComplete();
    static void s_onDeviceConnected(uni_hid_device_t *device);
    static void s_onDeviceDisconnected(uni_hid_device_t *device);
    static int s_onDeviceReady(uni_hid_device_t *device);
    static void s_onControllerData(uni_hid_device_t *device, uni_controller_t *controller);
    static int32_t s_getPlatformProperty(uni_platform_property_t key);
    static void s_onOobEvent(uni_platform_oob_event_t event, void *data);
    static void s_deviceDump(uni_hid_device_t *device);
    static void s_registerConsoleCommands();

    void init(int argc, const char **argv);
    void onInitComplete();
    void onDeviceConnected(uni_hid_device_t *device);
    void onDeviceDisconnected(uni_hid_device_t *device);
    int onDeviceReady(uni_hid_device_t *device);
    void onControllerData(uni_hid_device_t *device, uni_controller_t *gamepad);
    int32_t getPlatformProperty(uni_platform_property_t key);
    void onOobEvent(uni_platform_oob_event_t event, void *data);
    void deviceDump(uni_hid_device_t *device);
    void registerConsoleCommands();
};
