#include <math.h>

#include <motorset.hpp>

void MotorSet::goInDirection(float x, float y)
{
    // Calculate
    auto stickAngle = std::atan2<float, float>(y, x);
    auto stickMag = std::hypotf(x, y);
    // Map the magnitude from a unit square to a unit circle.
    auto stickMaxMag = 1.0f / abs(abs(stickAngle) > M_PI_4 && abs(stickAngle) < M_3PI_4 ? sinf(abs(stickAngle))
                                                                                        : cosf(abs(stickAngle)));

    // Different modes depending on the stick angle
    static constexpr float kAngleDelta = 1.0f * M_PI / 3.0f;
    static constexpr float kForwardAngle = M_PI_2;
    static constexpr float kForwardLeftAngle = kForwardAngle + kAngleDelta;
    static constexpr float kForwardRightAngle = kForwardAngle - kAngleDelta;
    static constexpr float kBackwardAngle = -M_PI_2;
    static constexpr float kBackwardLeftAngle = kBackwardAngle - kAngleDelta;
    static constexpr float kBackwardRightAngle = kBackwardAngle + kAngleDelta;

    static constexpr float kRotateAngleDelta = M_PI / 12.0f;
    static constexpr float kRotateLeftBottom = -M_PI + kRotateAngleDelta;
    static constexpr float kRotateLeftTop = M_PI - kRotateAngleDelta;
    static constexpr float kRotateRightBottom = -kRotateAngleDelta;
    static constexpr float kRotateRightTop = kRotateAngleDelta;

    float motorSpeedLeft = 0;
    float motorSpeedRight = 0;
    if(stickAngle >= kForwardRightAngle && stickAngle <= kForwardLeftAngle)
    {
        // Moving forward
        motorSpeedLeft = stickAngle > kForwardAngle ? 1 - (stickAngle - kForwardAngle) / kAngleDelta : 1;
        motorSpeedRight = stickAngle < kForwardAngle ? 1 - (kForwardAngle - stickAngle) / kAngleDelta : 1;
    }
    else if(stickAngle >= kBackwardLeftAngle && stickAngle <= kBackwardRightAngle)
    {
        // Moving backward
        motorSpeedLeft = stickAngle < kBackwardAngle ? -1 - (stickAngle - kBackwardAngle) / kAngleDelta : -1;
        motorSpeedRight = stickAngle > kBackwardAngle ? -1 - (kBackwardAngle - stickAngle) / kAngleDelta : -1;
    }
    else if(stickAngle > kRotateLeftTop || stickAngle < kRotateLeftBottom)
    {
        // rotating on the spot counter clock wise
        motorSpeedLeft = -1;
        motorSpeedRight = 1;
    }
    else if(stickAngle < kRotateRightTop && stickAngle > kRotateRightBottom)
    {
        // rotating on the spot clock wise
        motorSpeedLeft = 1;
        motorSpeedRight = -1;
    }

    // Apply the gain
    motorSpeedLeft *= -stickMag * 1024.0f / stickMaxMag;
    motorSpeedRight *= stickMag * 1024.0f / stickMaxMag;

    // Actuate
    leftFront.setSpeed(static_cast<int16_t>(motorSpeedLeft));
    leftBack.setSpeed(static_cast<int16_t>(motorSpeedLeft));
    rightFront.setSpeed(static_cast<int16_t>(motorSpeedRight));
    rightBack.setSpeed(static_cast<int16_t>(motorSpeedRight));
}
