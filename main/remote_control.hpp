#pragma once

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <bluepad_platform.hpp>
#include <motorset.hpp>

class RemoteControl
{
    static constexpr unsigned kModelPeriod = 10;
    static constexpr const char *kTag = "Remote";

public:
    RemoteControl(const MotorSet &motors, const ControllerState &controller);

    void start();
    void tick();
    void stop();

private:
    TickType_t _lastRunTime{0};

    MotorSet _motors;
    const ControllerState &_controller;
};
